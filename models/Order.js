const mongoose = require('mongoose');

let orderSchema = new mongoose.Schema({
	
	userID: {
		type: String,
		required: [true, "Order ID is required."]
	},
	purchasedProducts: [
		{
			productID: {
				type: String,
				required: [true, "Product ID is required."]
			},
			productName: {
				type: String,
				required: [true, "Product Name is required."]
			},
			productCategory: {
				type: String,
				required: [true, "Product Category is required."]
			},
			quantity: {
				type: Number,
				required: [true, "Ouantity is required."]
			},
			price: {
				type: Number,
				required: [true, "Error getting subtotal"]
			},
			subTotal: {
				type: Number,
				required: [true, "Error getting subtotal"]
			},
			dateOrdered: {
				type: Date,
				default: new Date()
			}
		}
	],
	totalQuantity: {
		type: Number,
		required: [true, "Error getting total quantity"]
	},
	totalAmount: {
		type: Number,
		required: [true, "Error getting total amount"]
	}
});

module.exports = mongoose.model("Order", orderSchema);
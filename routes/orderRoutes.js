const express = require('express');
const router = express.Router();

const orderControllers = require('../controllers/orderControllers');

const auth = require("../auth");

const {verify, verifyAdmin} = auth;

// ROUTES

// ADD ORDER
router.post("/", verify, orderControllers.addOrder);

// RETRIEVE LOGGED IN USER'S ORDERS
router.get("/cart", verify, orderControllers.checkOrders);

// RETRIEVE ALL ORDERS
router.get("/all", verify, verifyAdmin, orderControllers.getAllOrders);

// RETRIEVE SPECIFIC USER'S ORDERS
router.get("/:id", verify, verifyAdmin, orderControllers.checkUserOrders);

// REMOVE ORDER
router.put("/cancel", verify, orderControllers.removeOrder);

module.exports = router;
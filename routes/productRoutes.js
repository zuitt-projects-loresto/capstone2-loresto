const express = require('express');
const router = express.Router();

const productControllers = require('../controllers/productControllers');

const auth = require("../auth");

const {verify, verifyAdmin} = auth;

// Routes

// CREATE NEW PRODUCT
router.post("/", verify, verifyAdmin, productControllers.createProduct);

// GET ALL PRODUCTS
router.get("/all", verify, verifyAdmin, productControllers.getAllProducts);

// GET ALL ACTIVE PRODUCTS
router.get("/", productControllers.getAllActiveProducts);

// GET SINGLE PRODUCT
router.get("/getSingleProduct/:id", productControllers.getSingleProduct);

// UPDATE PRODUCT INFORMATION
router.put("/modifyProduct/:id", verify, verifyAdmin, productControllers.modifyProduct);

// ARCHIVE PRODUCT
router.put("/archive/:id", verify, verifyAdmin, productControllers.archiveProduct);

// UNARCHIVE PRODUCT
router.put("/unarchive/:id", verify, verifyAdmin, productControllers.unarchiveProduct);

// GET PRODUCTS BY NAME
router.post("/name", productControllers.getProductsByName);

// GET PRODUCTS BY CATEGORY
router.post("/category", productControllers.getProductsByCategory);

// GET PRODUCTS BY PRICE
router.post("/price", productControllers.getProductsByPrice);


module.exports = router;
const express = require('express');
const router = express.Router();

const userControllers = require('../controllers/userControllers');

const auth = require('../auth');

const {verify, verifyAdmin} = auth;


// ROUTES

// USER REGISTRATION
router.post("/", userControllers.registerUser);

// LOGIN USER
router.post("/login", userControllers.loginUser);

// GET ALL USERS
router.get("/", verify, verifyAdmin, userControllers.getAllUsers);

// REGULAR TO ADMIN
router.put("/updateAdmin/:id", verify, verifyAdmin, userControllers.updateAdmin);

// ADMIN TO REGULAR
router.put("/removeAdmin/:id", verify, verifyAdmin, userControllers.removeAdmin);



module.exports = router;
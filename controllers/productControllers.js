const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const auth = require('../auth');

// CREATE NEW PRODUCT
module.exports.createProduct = (req, res) => {

	console.log(req.body);

	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		category: req.body.category,
		price: req.body.price,
		stocks: req.body.stocks
	});

	newProduct.save()
	.then(product => res.send(product))
	.catch(err => res.send(err));
};

// GET ALL PRODUCTS
module.exports.getAllProducts = (req, res) => {

	Product.find({})
	.then(product => res.send(product))
	.catch(err => res.send(err));
};

//GET ALL ACTIVE PRODUCTS
module.exports.getAllActiveProducts = (req, res) => {

	Product.find({isActive: true})
	.then(product => res.send(product))
	.catch(err => res.send(err));
};

//GET SINGLE PRODUCT
module.exports.getSingleProduct = (req, res) => {

	console.log(req.params.id);

	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));

};

// UPDATE PRODUCT INFORMATION
module.exports.modifyProduct = (req, res) => {
	
	console.log(req.params.id);
	let updates = {
	 	name: req.body.name,
		description: req.body.description,
		category: req.body.category,
		price: req.body.price,
		stocks: req.body.stocks
	};

	Product.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(modifiedProduct => res.send(modifiedProduct))
	.catch(err => res.send(err));
};

// ARCHIVE PRODUCT
module.exports.archiveProduct = (req, res) => {

	let updates = {isActive: false}
	Product.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(product => {

		console.log(product)
		return res.send(`${product.name} is now removed from the menu.`)
	})
	.catch(err => res.send(err));
}

// UNARCHIVE PRODUCT
module.exports.unarchiveProduct = (req, res) => {

	let updates = {isActive: true}
	Product.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(product => {

		console.log(product)
		return res.send(`${product.name} is now back to the menu.`)
	})
	.catch(err => res.send(err));
}

// GET PRODUCTS BY NAME
module.exports.getProductsByName = (req, res) => {

	console.log(req.body);

	Product.find({
		name : {$regex: req.body.name, $options: '$i'}
	})
	.then(result => {

		if(result.length == 0){
			return res.send("No products found")
		} else {
			return res.send(result)
		}
	})
	.catch(err => res.send(err));

};

// GET PRODUCTS BY CATEGORY
module.exports.getProductsByCategory = (req, res) => {

	console.log(req.body);

	Product.find({
		category : {$regex: req.body.category, $options: '$i'}
	})
	.then(result => {

		if(result.length == 0){
			return res.send("No products found")
		} else {
			return res.send(result)
		}
	})
	.catch(err => res.send(err));

};

// GET PRODUCTS BY PRICE
module.exports.getProductsByPrice = (req, res) => {

	let values = {
		min : req.body.min,
		max : req.body.max
	}

	if(req.body.min === undefined){req.body.min= 0};
	if(req.body.max === undefined){req.body.max= 99999};

	console.log(req.body.min);
	console.log(req.body.max);

	Product.find({
		$and : [
			{price: {$lte : req.body.max}},
			{price: {$gte : req.body.min}}
		]
	})
	.then(result => {

		console.log(result)

		if(result.length === 0){
			return res.send("No product found.")
		} else {
			return res.send(result);
		}

	})
	.catch(err => res.send(err))
};
const bcrypt = require('bcrypt');
const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const auth = require('../auth');

// USER REGISTRATION
module.exports.registerUser = (req, res) => {

	console.log(req.body);

	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	let newUser = new User({
		email: req.body.email,
		password: hashedPW
	})

	User.findOne({email: req.body.email})
	.then(result => {

		if(result !== null && result.name === req.body.name){

			return res.send("Email is already registered!")

		} else {

			newUser.save()
			.then(user => res.send(user))
			.catch(err => res.send(err));
		
		}
	})
	.catch(err => res.send(err));
}

// LOGIN USER
module.exports.loginUser = (req, res) => {

	console.log(req.body);

	User.findOne({email: req.body.email})
	.then(foundUser => {

		if(foundUser === null){

			return res.send("User does not exist.");
		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

			if(isPasswordCorrect) {

				return res.send({accessToken: auth.createAccessToken(foundUser)})
			} else {

				return res.send("Sorry, wrong password.")
			}
		}
	})
	.catch(err => res.send(err));
};

// GET ALL USERS
module.exports.getAllUsers = (req, res) => {

	User.find({})
	.then(user => res.send(user))
	.catch(err => res.send(err));
};

// REGULAR TO ADMIN
module.exports.updateAdmin = (req, res) => {

	console.log(req.user.id);
	console.log(req.params.id);

	let updates = {
		isAdmin : true
	}

	let isUserUpdated = User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(user =>  {
			return res.send({
				auth: "Success!",
				message: `${user.email} is now an Admin.`
			})
		})
	.catch(err => res.send(err));
};

// ADMIN TO REGULAR
module.exports.removeAdmin = (req, res) => {

	console.log(req.user.id);
	console.log(req.params.id);

	let updates = {
		isAdmin : false
	}

	let isUserUpdated = User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(user =>  {
			return res.send({
				auth: "Success!",
				message: `${user.email} is now REMOVED as an Admin.`
			})
		})
	.catch(err => res.send(err));
};
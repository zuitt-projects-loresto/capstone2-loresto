const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const auth = require('../auth');

// ADD ORDER
module.exports.addOrder = (req, res) => {

	if(req.user.isAdmin){
		return res.send("Action Forbidden");
	};

	Product.findById(req.body.productID)
	.then(product => {
		let productPrice = product.price;
		let itemName = product.name;
		let itemCategory = product.category;
		let itemLeft = product.stocks;
		let computeSubTotal = req.body.quantity * productPrice;
		let computeTotalQuantity = req.body.quantity;
		let computeTotalAmount = computeSubTotal;
		console.log(product.stocks);

		if(req.body.quantity > itemLeft){
			return res.send(`${product.name}(${product.category}) has ${product.stocks} stock(s) left.`)
		} else {
			Order.findOne({userID: req.user.id})
			.then(order => {

				console.log(order);

				if(order === null){

					let newOrder = new Order({
						userID: req.user.id,
						purchasedProducts:[
							{
								productID: req.body.productID,
								productName: itemName,
								productCategory: itemCategory,
								quantity: req.body.quantity,
								price: productPrice,
								subTotal: computeSubTotal
							}
						],
						totalQuantity: computeTotalQuantity,
						totalAmount: computeSubTotal
					});

					console.log(newOrder);
					return newOrder.save()
					.then(order => {
						
						newCount = product.stocks - req.body.quantity
						let newInventoryUpdate = {
							stocks: newCount
						}
						Product.findByIdAndUpdate(req.body.productID, newInventoryUpdate, {new: true})
						.then(order => res.send({message: 'Orders added successfully! Order more!'}))
						.catch(err => err.message);
					})
					.catch(err => err.message);
				} else if(order !== null){
					
					let additionalOrder = {
						productID: req.body.productID,
						productName: itemName,
						productCategory: itemCategory,
						quantity: req.body.quantity,
						price: productPrice,
						subTotal: computeSubTotal
					};

					order.purchasedProducts.push(additionalOrder);

					return order.save()
					.then(order => {

						let previousTotalQuantity = order.totalQuantity;
						let previousTotalAmount = order.totalAmount;
						let newOrderQty = req.body.quantity;
						computeSubTotal = newOrderQty * productPrice;
						let newTotalQuantity = previousTotalQuantity + newOrderQty;
						let newTotalAmount = previousTotalAmount + computeSubTotal;
						console.log(newTotalQuantity);
						console.log(newTotalAmount);

						let amountUpdates = {
							totalQuantity: newTotalQuantity,
							totalAmount: newTotalAmount
						}
						Order.findOneAndUpdate({userID: req.user.id}, amountUpdates, {new:true})
						.then(order => {
							
							newCount = product.stocks - req.body.quantity
							let newInventoryUpdate = {
								stocks: newCount
							}
							Product.findByIdAndUpdate(req.body.productID, newInventoryUpdate, {new: true})
							.then(order => res.send({message: 'Orders added successfully! Order more!'}))
							.catch(err => err.message);
						})
						.catch(err => err.message);
					})
				} else {
					return res.send({message: 'recheck orders'});
				}
				return res.send({message: 'recheck orders'});
			})
			.catch(err => err.message);
		}
		return res.send(order);
	})
	.catch(err => err.message);
}

// RETRIEVE LOGGED IN USER'S ORDER
module.exports.checkOrders = (req, res) => {

	Order.findOne({userID: req.user.id})
	.then(order => {

		return res.send(order)
	})
	.catch(err => res.send(err));
};

// RETRIEVE ALL ORDERS
module.exports.getAllOrders = (req, res) => {

	Order.find({})
	.then(order => res.send(order))
	.catch(err => res.send(err));
};

// RETRIEVE SPECIFIC USER'S ORDER
module.exports.checkUserOrders = (req, res) => {

	Order.findOne({userID: req.params.id})
	.then(order => {

		return res.send(order)
	})
	.catch(err => res.send(err));
};

// REMOVE ORDER
module.exports.removeOrder = (req, res) => {
	
	if(req.user.isAdmin){
		return res.send("Action Forbidden");
	}

	let remove = {
			purchasedProducts : {_id: req.body._id}
		}

	let index = 0
	let cancelQuantity = 0
	let cancelSubTotal = 0
	let findOrder = req.body._id
	let compare = null
	console.log(findOrder);
	console.log(req.user.id); //ok
	console.log(req.body._id); //ok

	Order.findOne({userID: req.user.id})
	.then(order => {

		while(findOrder !== compare){
			const {productID, productName, quantity, price, subTotal, dateOrdered, _id} = order.purchasedProducts[index]
			console.log(productID);
			console.log(productName);
			console.log(quantity);
			console.log(price);
			console.log(subTotal);
			console.log(dateOrdered);
			console.log(_id);
			console.log("hi");
			index = index + 1;
			console.log(index);
			compare = _id
			cancelQuantity = quantity
			cancelSubTotal = subTotal

			if(findOrder == compare){

				let removeOrderQty = cancelQuantity
				let removeOrderAmt = cancelSubTotal
				let previousTotalQuantity = order.totalQuantity;
				let previousTotalAmount = order.totalAmount;	
				console.log(removeOrderQty); //2
				console.log(removeOrderAmt); //98
				console.log(previousTotalQuantity); //10
				console.log(previousTotalAmount); //590

				Order.findOneAndUpdate({userID: req.user.id}, {$pull: remove}, {new: true})
				.then(order => {
					let newTotalQuantity = previousTotalQuantity - removeOrderQty;
					let newTotalAmount = previousTotalAmount -removeOrderAmt;
					console.log(newTotalQuantity);
					console.log(newTotalAmount);

					let amountUpdates = {
						totalQuantity: newTotalQuantity,
						totalAmount: newTotalAmount
					}
					Order.findOneAndUpdate({userID: req.user.id}, amountUpdates, {new:true})
					.then(order => res.send({message: 'Orders removed successfully!'}))
					.catch(err => err.message);	
				})
			}
		}
	})
	.catch(err => res.send(err));
};